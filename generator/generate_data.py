import sys
import random

def main(argv):
    file_name = argv[1]
    lines_count = int(argv[2])
    timestamp = 1510670916247
    
    # opening file from argv[1]
    with open(file_name, 'w') as f:
        for i in range(lines_count):
            # id generating
            id = random.randint(1, 15)
            # incrementing timestamp
            timestamp += 2
            # value generating 
            value = int(random.normalvariate(300, 50))
            # line in csv-format
            line = str(id) + ',' + str(timestamp) + ',' + str(value) + '\n'
            f.write(line)

if __name__ == "__main__":
    main(sys.argv)
