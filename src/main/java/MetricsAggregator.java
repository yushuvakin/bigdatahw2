import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;


public final class MetricsAggregator {
    private static String hostWithPort = "localhost:9092";
    private static int metric = 10;

    public static void main(String[] args) throws Exception {
        // checking arguments count
        if (args.length != 1) {
            System.out.println("Enter path to file");
        }

        // creating spark session
        SparkSession sparkSession = SparkSession
                .builder()
                .appName("MetricsAggregator")
                .getOrCreate();

        String fileName = args[0];
        // parsing csv file into dataset
        Dataset<Row> allDataset = sparkSession.read().csv(fileName);
        // changing column names for our dataset
        Dataset<Row> allDatasetRenamed = allDataset.toDF("id", "timestamp", "val");

        // renaming table which associated with dataset
        allDatasetRenamed.createOrReplaceTempView("metrics");

        // grouping data and calculating metrics
        Dataset<Row> groupedDataset = sparkSession.sql("SELECT id, first(timestamp), AVG(val/"
                + metric + ") as average_val FROM metrics GROUP BY id");

        // adding column with indicator of metrics
        Dataset<Row> datasetWithMetric = groupedDataset.withColumn("m",
                lit(new StringBuilder().append(metric).append("M").toString()));

        // rearranging dataset columns
        Dataset<Row> rearrangedDataset = datasetWithMetric.select(col("id"),
                col("first(timestamp, false)"), col("m"), col("average_val"));

        // aggregating dataset into csv-format string
        Dataset<String> aggregatedDataset = rearrangedDataset.map((MapFunction<Row, String>) s ->
                s.get(0) + "," + s.get(1) + "," + s.get(2) + "," + s.get(3),
                Encoders.STRING());

        // writing data in Kafka
        aggregatedDataset.write()
                .format("kafka")
                .option("kafka.bootstrap.servers", hostWithPort)
                .option("topic", "metrics")
                .save();

        // reading data from Kafka
        Dataset<Row> datasetFromKafka = sparkSession.read()
                .format("kafka")
                .option("kafka.bootstrap.servers", hostWithPort)
                .option("subscribe", "metrics")
                .load();

        // printing data from Kafka on screen
        datasetFromKafka.selectExpr("CAST(value AS STRING)").show(false);
    }
}