import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import org.junit.Before;

import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MetricsAggregatorTests {
    @Mock
    SparkSession sparkSession;

    @Mock
    Dataset<Integer> datasetWithMetric;

    String hostWithPort = "localhost:9092";
    int metric = 10;

    // set up test
    @Before
    public void setUp() {
        // initialization of metric
        metric = 10;
    }

    // testing renaming columns
    @Test
    public void testRenamingColumns() {
        datasetWithMetric.toDF("id", "timestamp", "val");
        verify(datasetWithMetric).toDF("id", "timestamp", "val");
    }

    // testing sql execution
    @Test
    public void testSqlExecution() {
        String sqlString = "SELECT id, first(timestamp), AVG(val/"
                + metric + ") as average_val FROM metrics GROUP BY id";
        sparkSession.sql(sqlString);
        verify(sparkSession).sql(sqlString);
    }

    // test writing in Kafka
    @Test
    public void testWritingInKafka() {
        datasetWithMetric.write();
        verify(datasetWithMetric).write();
    }
}
